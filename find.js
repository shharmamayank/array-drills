function find(arrays, callback) {
    if (!Array.isArray(arrays) || !callback) {
        return [];
    } else {
        for (let index = 0; index < arrays.length; index++) {
            if (callback(arrays[index])) {
                return arrays[index];
            }
        }
    }

}
module.exports = find